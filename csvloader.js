function onFileOpen(event) {
    event.data.fileRef.file = event.target.files[0];
}

function onFileError(event) {
    alert('ファイルの読み込みに失敗しました');
}

function onGenerateFromFile(event) {
    var file = event.data.fileRef.file;
    var reader = new FileReader();
    reader.readAsText(file);
    reader.onload = function (evt) {
        var csvText = evt.target.result;
        event.data.dataRef.Data = [];
        var lines = csvText.split(/\r\n|\n/);
        for (var i=0; i < lines.length; ++i) {
            var line = lines[i].split(',');
            if (line[0].substr(0, 1) === '#') {
                continue;
            }
            if (line.length >= 3) {
                var y = Number(line[2]);
                if (y !== 1) {
                    y = 0;
                }
                var data = {x:[Number(line[0]), Number(line[1])], y:y};
                event.data.dataRef.Data.push(data);
            }
        }

        var canvas = event.data.canvas;
        var color_t = event.data.color_t;
        var color_f = event.data.color_f;
        var range = event.data.range;
        canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
        drawPoints(event.data.dataRef.Data, canvas, range, color_t, color_f);
        alert('データを読み込みました')
    }
    reader.onerror = onFileError;
}

function onDownload(event) {
    var bom = new Uint8Array([0xEF, 0xBB, 0xBF]);
    var content = '# x座標,y座標,ラベル\n';
    var data = event.data.dataRef.Data;
    if (data != null) {
        for (var i=0; i < data.length; ++i) {
            content += data[i].x[0] + ',' + data[i].x[1] + ',' + data[i].y + '\n';
        }
    }
    var blob = new Blob([bom, content], {"type": "text/csv"});
    if (window.navigator.msSaveBlob) {
        window.navigator.msSaveBlob(blob, "training_data.csv");
        window.navigator.msSaveOrOpenBlob(blob, "training_data.csv");
    } else {
        document.getElementById("download").href = window.URL.createObjectURL(blob);
    }
}
