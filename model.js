/* データの生成モデル */
var Model = (function() {
    /* コンストラクタ */
    var Model = function(a, w) {
        this.activate = a;
        this.w = w;     //重み
    }

    var pt = Model.prototype;

    /* モデル関数
       * x_1: 入力データ(x_1座標値)
       */
    pt.h = function(x_1) {
        return -(this.w[1] * x_1 + this.w[0]) / this.w[2];
    }

    pt.sum = function(x) {
        var ret = 0;
        for (var i=0; i < this.w.length; ++i) {
            ret += this.w[i] * phi(x, i);
        }
        return ret;
    }

    pt.out = function(x) {
        return this.activate(this.sum(x));
    }

    pt.judge = function (x) {
        return step(this.sum(x));
    }

    return Model;
})();

function step(x) {
    if (x >= 0) {
        return 1;
    } else {
        return 0;
    }
}

function sigmoid(x) {
    return 1 / (1 + Math.exp(-x));
}

function phi(x, i) {
    var x_added = [1].concat(x);
    return x_added[i];
}
