$(function () {
    var RANGE = [20, 20];   //座標系の定義域幅
    (function () {
        var axesCanvas = $('#axesCanvas')[0];
        drawAxes(axesCanvas, RANGE);
    })();

    var dataCanvas = $('#dataCanvas')[0];
    var modelCanvas = $('#modelCanvas')[0];
    var COLOR_T = 'rgb(255, 0, 0)';     //訓練データモデル曲線色
    var COLOR_F = 'rgb(0, 0, 255)';     //学習モデル曲線色

    /* 訓練データの設定 */
    var dataRef = {Data:null};
    var fileRef = {file:null};

    /* イベントハンドラの設定 */
    $('#gen').on('click', {dataRef:dataRef, canvas:dataCanvas, range:RANGE, color_t:COLOR_T, color_f:COLOR_F}, onGenerate);
    $('#resetButton').on('click', {dataRef:dataRef, dataCanvas:dataCanvas, modelCanvas:modelCanvas}, onReset);
    $('#genFromFile').on('click', {dataRef:dataRef, fileRef:fileRef, canvas:dataCanvas, range:RANGE, color_t:COLOR_T, color_f:COLOR_F}, onGenerateFromFile);
    $('#download').on('click', {dataRef:dataRef}, onDownload);
    $('#openFile').on('change', {fileRef:fileRef}, onFileOpen);
    $('#learn').on('click', {dataRef:dataRef, canvas:modelCanvas, range:RANGE, color:COLOR_F}, onTrain);
    $('#dataCanvas').on('click', {dataRef:dataRef, canvas:dataCanvas, range:RANGE, color_t:COLOR_T, color_f:COLOR_F}, onCanvasClicked);
    $(window).on('keydown', onShiftkeyDown);
    $(window).on('keydown', onEnterkeyDown);
});

function onShiftkeyDown(event) {
    if (event.shiftKey) {
        if($('input[name="clickLabel"]:eq(0)').prop('checked')) {
            $('input[name="clickLabel"]:eq(1)').prop('checked', true);
        } else {
            $('input[name="clickLabel"]:eq(0)').prop('checked', true);
        }
    }
}

function onEnterkeyDown(event) {
    if (event.which === 13) {
        console.log('Enter');
        $('#learn').trigger('click');
    }
}

function onGenerate(event) {
    var N_t = Number($('#N_t').val());
    var w_t = [];
    var range = event.data.range;
    var canvas = event.data.canvas;
    var color_t = event.data.color_t;
    var color_f = event.data.color_f;
    $(".w_t").each(function(i, elem) {
        w_t[i] = Number($(elem).val());
    });

    var m_t = new Model(step, w_t);
    var trGenerator = new TrainingData(m_t, N_t, range);
    event.data.dataRef.Data = trGenerator.Data;

    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
    drawFunction(m_t, canvas, range, color_t);
    drawPoints(event.data.dataRef.Data, canvas, range, color_t, color_f);
}

function onTrain(event) {
    alert('学習開始');
    var w_l = [];
    var eta = Number($('#eta').val());
    var alpha = Number($('#alpha').val());
    var itr = Number($('#itr').val());
    var canvas = event.data.canvas;
    var range = event.data.range;
    var color = event.data.color;
    $(".w_l").each(function(i, elem) {
        w_l[i] = Number($(elem).val());
    });

    var m_l = new Model(sigmoid, w_l);
    var l = new LogisticRegression(m_l, eta, alpha, itr);
    l.train(event.data.dataRef.Data);

    var out = $('#output span');
    out.children().remove();
    for (var i=0; i < w_l.length; ++i) {
        var w = m_l.w[i];
        var w_str;
        if (Math.abs(w) >= 1 / 100) {
            w_str = w.toFixed(3);
        } else {
            w_str = w.toExponential(3);
        }
        var htmlText = '$w_{' + i + '} = ' + w_str + '$';
        if (i !== w_l.length - 1) {
            htmlText += ', ';
        }
        var w_span = $('<span>').html(htmlText);
        out.append(w_span);
    }
    var error_str = String(l.check(event.data.dataRef.Data));
    out.append($('<sapn>').html('<br />$N_{error} = ' + error_str + '$'));
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, out.attr('id')]);

    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
    drawDiscriminant(canvas, range, m_l);
}

function onCanvasClicked(event) {
    var canvas = event.data.canvas;
    var range = event.data.range;
    var canvasRect = canvas.getBoundingClientRect();
    var color_t = event.data.color_t;
    var color_f = event.data.color_f;
    var cx = event.clientX - canvasRect.left;
    var cy = event.clientY - canvasRect.top;
    var c = [cx, cy];
    var x = canvasToData(c, canvas, range);
    var y = ($('input[name="clickLabel"]:checked').val() === "positive") ? 1: 0;
    var d = {x:x, y:y};

    if (event.data.dataRef.Data === null) {
        event.data.dataRef.Data = [];
    }
    event.data.dataRef.Data.push(d);
    drawPoint(d, canvas, range, color_t, color_f);
}

function onReset(event) {
    event.data.dataRef.Data = [];
    var dataCanvas = event.data.dataCanvas;
    var modelCanvas = event.data.modelCanvas;
    dataCanvas.getContext('2d').clearRect(0, 0, dataCanvas.width, dataCanvas.height);
    modelCanvas.getContext('2d').clearRect(0, 0, modelCanvas.width, modelCanvas.height);
    out = $('#output span');
    out.children().remove();
}
