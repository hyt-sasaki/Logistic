var TrainingData = (function () {
    /* コンストラクタ */
    var TrainingData = function (m, N, r) {
        this.model = m;       //訓練データ生成モデル
        this.N_t = N;       //訓練データ数
        this.range = r;   //座標の定義域幅

        this.generate();    //訓練データの生成
    }

    var pt = TrainingData.prototype;

    /* 訓練データ生成 */
    pt.generate = function() {
        this.Data = [];    //訓練データ集合
        for (var i=0; i < this.N_t; ++i) {
            var x_1 = Math.random() * this.range[0] - this.range[0] / 2;
            var x_2 = Math.random() * this.range[1] - this.range[1] / 2;
            var x = [x_1, x_2];
            this.Data.push({x:x, y:this.model.judge(x)});
        }
    }

    return TrainingData;
})();
