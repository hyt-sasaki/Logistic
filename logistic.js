var LogisticRegression = (function () {
    /* コンストラクタ */
    var LogisticRegression = function (m, e, a, i) {
        this.model = m; //学習モデル
        this.eta = e; //学習率
        this.alpha = a;   //学習率のスケジューリングファクター
        this.itr = i;   //学習回数
    }

    var pt = LogisticRegression.prototype;

    /* 学習 */
    pt.train = function (Data) {
        var choice = [];
        for (var i=0; i < Data.length; ++i) {
            choice[i] = i;
        }

        for (var i=0; i < this.itr; ++i) {
            var c = choice.slice();
            while(c.length > 0) {
                var rnd = Math.floor(Math.random() * c.length);
                var d = Data[c[rnd]];

                c.splice(rnd, 1);

                this.update(d);
            }
            this.eta *= this.alpha;
        }
    }

    /* 更新 */
    pt.update = function (d) {
        w_0 = this.model.w.slice(); //重みのコピー
        for (var m=0; m < w_0.length; ++m) {
            w_0[m] += this.eta * (d.y - this.model.out(d.x)) * phi(d.x, m);
        }
        this.model.w = w_0;
    }

    /* エラーチェック */
    pt.check = function (Data) {
        var error = 0;
        for (var i=0; i < Data.length; ++i) {
            if (this.model.judge(Data[i].x) !== Data[i].y) {
                error++;
            }
        }
        return error;
    }

    return LogisticRegression;
})();
